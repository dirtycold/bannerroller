#-------------------------------------------------
#
# Project created by QtCreator 2015-06-12T16:25:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testBanner
TEMPLATE = app


SOURCES += main.cpp\
        bannerrollerwidget.cpp \
    bannerroller.cpp

HEADERS  += bannerrollerwidget.h \
    bannerroller.h
