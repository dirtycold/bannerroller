#ifndef BANNERROLLER_H
#define BANNERROLLER_H

#include <QtCore/QObject>
#include <QtCore/QStringList>

class BannerRoller : public QObject
{
    Q_OBJECT
public:
    explicit BannerRoller(QObject *parent = 0);

public slots:
    const QString pick () const;

private:
    void load ();
    QStringList list;
};

#endif // BANNERROLLER_H
