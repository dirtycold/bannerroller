#include "bannerroller.h"
#include <QFile>
#include <QTextStream>
#include <QTime>
#include <cmath>

BannerRoller::BannerRoller(QObject *parent) : QObject(parent)
{
    load ();
}

const QString BannerRoller::pick() const
{
    if (list.isEmpty ())
        return QString ("没有数据呀");
    QTime time = QTime::currentTime ();
    unsigned int seed = time.msec ();
    srand (seed);
    double _index = double (rand ()) * double (list.size () - 1) / double (RAND_MAX);
    unsigned int index = round (_index);
    return list.at (index);
}

void BannerRoller::load()
{
    list.clear ();
    QFile file("data.txt");
    file.open (QIODevice::ReadOnly);
    QTextStream stream (&file);
    while (!stream.atEnd ())
    {
        list << stream.readLine ();
    }
    file.close ();
}

