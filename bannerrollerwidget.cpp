#include "bannerrollerwidget.h"
#include <QLayout>
#include <QLabel>
#include <QFont>
#include <QtDebug>

BannerRollerWidget::BannerRollerWidget(QWidget *parent)
    : QWidget(parent)
{
    QVBoxLayout *layout = new QVBoxLayout (this);
    label = new QPushButton ("放马过来", this);
    label->setFlat (true);
    label->setMinimumHeight (480);
    QFont font (label->font ());
    font.setPixelSize (480);
    label->setFont (font);
    layout->addWidget (label);
    setLayout (layout);
    connect (label, &QPushButton::clicked, this, &BannerRollerWidget::getBanner);
}

BannerRollerWidget::~BannerRollerWidget()
{

}

void BannerRollerWidget::getBanner()
{
    QString banner = roller.pick ();
    label->setText (banner);
}
