#ifndef BANNERROLLERWIDGET_H
#define BANNERROLLERWIDGET_H

#include <QWidget>
#include <QPushButton>
#include "bannerroller.h"

class BannerRollerWidget : public QWidget
{
    Q_OBJECT

public:
    BannerRollerWidget(QWidget *parent = 0);
    ~BannerRollerWidget();

    void getBanner ();

private:
    QPushButton *label;
    unsigned short trial;
    BannerRoller roller;
    QString lastBanner;
};

#endif // BANNERROLLERWIDGET_H
